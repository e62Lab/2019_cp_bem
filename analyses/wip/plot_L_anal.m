prepare

[X, Y] = meshgrid(deal(linspace(-1, 1)));
I = X > 0 & Y < 0;
X(I) = nan;
Y(I) = nan;

omega = 3*pi/2;
V = L_anal(X, Y, omega);

setfig('b1');
contourf(X, Y, V, 100, 'EdgeColor', 'none');
axis equal
colorbar

setfig('b3');
view(3);
surf(X, Y, V, 'EdgeColor', 'none');
axis equal
colorbar