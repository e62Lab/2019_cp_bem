prepare

casename = 'L_shape_uniform_wip';
% casename = 'L_shape_adaptive_wip';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

omega = 3*pi/2;

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(x);
    sol = h5read(file, [gname '/sol']);
    asol = L_anal(x, y, omega);
    
    Ns(g) = N;
    
    errors(g, l1) = norm(sol-asol, 1) / norm(asol, 1);
    errors(g, l2) = norm(sol-asol, 2) / norm(asol, 2);
    errors(g, linf) = norm(sol-asol, 'inf') / norm(asol, 'inf');
end

setfig('b1');
ll;
leg = {};
for i = 1:nerr
    plot(Ns, errors(:, i), 'o-'); leg{end+1} = errleg{i};
end
legend(leg);