prepare

% casename = 'osc2d_uniform_wip';
casename = 'osc2d_adaptive_wip';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

alpha = h5readatt(file, '/conf', 'case.alpha');

den = 200;
[X, Y] = meshgrid(deal(linspace(0, 1, den)));

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(x);
    try
    sol = h5read(file, [gname '/sol']);
    catch, break; end
    
    if g == ng
setfig('b1');view(3);
scatter3(x, y, sol, 10, sol, 'filled');
colorbar
    end
    
    R = sqrt(X.^2 + Y.^2);
    A = sin(1./(alpha+R));
    asol = A(:);
    
    V = griddata(x, y, sol, X, Y);
    sol = V(:);
    
    
    Ns(g) = N;
    errors(g, l1) = norm(sol-asol, 1) / norm(asol, 1);
    errors(g, l2) = norm(sol-asol, 2) / norm(asol, 2);
    errors(g, linf) = norm(sol-asol, 'inf') / norm(asol, 'inf');
    
end

setfig('b3');
ll;
leg = {};
for i = 1:nerr
    plot(Ns, errors(:, i), 'o-'); leg{end+1} = errleg{i};
end
legend(leg);