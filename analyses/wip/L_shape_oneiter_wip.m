prepare

casename = 'L_shape_wip';
file = [datapath casename '.h5'];
info = h5info(file);

iter = '/003';

pos = h5read(file, [iter '/domain/pos']);
x = pos(:, 1);
y = pos(:, 2);
sol = h5read(file, [iter '/sol']);

setfig('b1');
scatter(x, y, 15, sol, 'filled');
colorbar
axis equal
title('Solution');

setfig('b3');
[~, d] = knnsearch(pos, pos, 'K', 2);
d = d(:, 2);
scatter(x, y, 15, log(d)/log(max(d)), 'filled');
colorbar
axis equal
title('Node density');