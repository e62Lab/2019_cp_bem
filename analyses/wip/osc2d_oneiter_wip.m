prepare

casename = 'osc2d_adaptive5_wip';
file = [datapath casename '.h5'];
info = h5info(file);

iter = '/007';

alpha = h5readatt(file, '/conf', 'case.alpha');
pos = h5read(file, [iter '/domain/pos']);
x = pos(:, 1);
y = pos(:, 2);
sol = h5read(file, [iter '/sol']);

setfig('b1');
scatter(x, y, 15, sol, 'filled');
colorbar
axis equal
title('Solution');


X = linspace(0, 1, 1000);
Y = X;

V = griddata(x, y, sol, X, Y);

setfig('a4');
plot(X, V);
plot(X, sin(1./(X+alpha)));
