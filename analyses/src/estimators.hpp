#ifndef COMPRESSED_DISK_ESTIMATORS_HPP
#define COMPRESSED_DISK_ESTIMATORS_HPP

#include <medusa/Medusa_fwd.hpp>

class DeviationEstimator {
    template < typename values_t>
    double get_deviation(const Range<int>& ind, const values_t& solution) {
        double mu = 0;
        for (int i : ind) {
            mu += solution[i];
        }
        mu /= ind.size();

        double var = 0;
        for (int i : ind) {
            var += (solution[i] - mu)*(solution[i] - mu);
        }
        return std::sqrt(var);
    }
  public:

    /**
     * Returns an estimate of error for each domain node.
     * @param domain The discretization of the domain.
     * @param solution Approximated solution in domain nodes.
     * @return An approximation of error in domain nodes.
     */
    VectorXd estimate(const DomainDiscretization<Vec3d>& domain, const std::pair<VectorField3d, VectorField<double, 6>>& sol) {
        auto stress = sol.second;
        int N = domain.size();
        VectorXd error = VectorXd::Zero(N);

        assert_msg(N == stress.rows(), "Solution size %d must match domain size %d.", stress.rows(), N);
        int d = stress.cols();

        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < d; ++j) {
                error(i) += get_deviation(domain.support(i), stress.c(j));
            }
        }
        return error;
    }

    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) += get_deviation(domain.support(i), sol);
        }
        return error;
    }
};

template <typename func_t>
class AnalyticalEstimator {

    func_t anal;
  public:
    AnalyticalEstimator(func_t anal) : anal(anal) {}

    template <typename vec_t>
    VectorXd estimate(const DomainDiscretization<vec_t>& domain, const VectorXd& sol) {
        int N = domain.size();
        assert_msg(N == sol.size(), "Solution size %d must match domain size %d.", sol.size(), N);

        VectorXd error = VectorXd::Zero(N);
        for (int i = 0; i < N; ++i) {
            error(i) = std::abs(anal(domain.pos(i)) - sol(i)) * domain.dr(i);
        }
        return error;
    }
};

#endif //COMPRESSED_DISK_ESTIMATORS_HPP
