#ifndef ADAPTIVE3D_POINT_CONTACT_CASE_HPP
#define ADAPTIVE3D_POINT_CONTACT_CASE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;

struct LshapeSolver {

    static double analytical(const Vec2d& p) {
        double r = p.norm();
        double phi = std::fmod(std::atan2(p[1], p[0])+2*PI, 2*PI);
        double omega = 3.0*PI/2.0;
        double alpha = PI / omega;
        return std::pow(r, alpha) * std::sin(alpha*phi);
    }

    static void preprocess(XML& conf) {}

    template<typename vec_t>
    Eigen::VectorXd solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& file, Timer& timer) {
        int basis_size = conf.get<int>("approx.m");
        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        string basis = conf.get<string>("approx.basis_type");

        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "phs") {
            int k = conf.get<int>("approx.k");
            int aug = conf.get<int>("approx.aug");
            RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw "";
    }

    template<typename vec_t, typename approx_t>
    Eigen::VectorXd solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& file, Timer& timer,
                           const approx_t& approx) {
        int N = d.size();
        prn(N);

        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.computeShapes(approx);

        timer.addCheckPoint("matrix");
        prn("matrix");

        SparseMatrix<double, RowMajor> M(N, N);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

        auto op = storage.implicitOperators(M, rhs);
        M.reserve(storage.supportSizes());

        for (int i : d.interior()) {
            op.lap(i) = 0;
        }
        for (int i : d.boundary()) {
            op.value(i) = analytical(d.pos(i));
        }

//        out_file.atomic().writeSparseMatrix("M", M);
//        out_file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);

        timer.addCheckPoint("postprocess");
        file.atomic().writeEigen("sol", sol);

        return sol;
    }

};


#endif //ADAPTIVE3D_POINT_CONTACT_CASE_HPP
