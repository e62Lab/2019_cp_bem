#ifndef ADAPTIVE3D_POINT_CONTACT_CASE_HPP
#define ADAPTIVE3D_POINT_CONTACT_CASE_HPP

#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/PardisoSupport>

using namespace mm;
using namespace std;
using namespace Eigen;

template <typename vec_t>
struct OscSolver {

    static double analytical(const vec_t& p, double alpha) {
        double r = p.norm();
        return std::sin(1.0/(alpha+r));
    }

    static void preprocess(XML& conf) {
        conf.set("case.alpha", 1.0/PI/conf.get<int>("case.nosc"));
    }

    Eigen::VectorXd solve(DomainDiscretization<vec_t>& domain, const XML& conf, HDF& file, Timer& timer) {
        int basis_size = conf.get<int>("approx.m");
        double sigmaB = conf.get<double>("approx.sigmaB");
        double sigmaW = conf.get<double>("approx.sigmaW");
        string basis = conf.get<string>("approx.basis_type");

        if (basis == "gau") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Gaussian<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<Multiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "phs") {
            int k = conf.get<int>("approx.k");
            int aug = conf.get<int>("approx.aug");
            RBFFD<Polyharmonic<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(k, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "imq") {
            int aug = conf.get<int>("approx.aug");
            RBFFD<InverseMultiquadric<double>, vec_t, ScaleToClosest, Eigen::PartialPivLU<Eigen::MatrixXd>> engine(sigmaB, aug);
            return solve_(conf, domain, file, timer, engine);
        } else if (basis == "mon") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        } else if (basis == "mont") {
            int m = conf.get<int>("approx.m");
            WLS<Monomials<vec_t>, GaussianWeight<vec_t>, ScaleToClosest> approx(Monomials<vec_t>::tensorBasis(m), sigmaW);
            return solve_(conf, domain, file, timer, approx);
        }
        assert_msg(false, "Unknown basis type '%s'.", basis);
        throw "";
    }

    template<typename approx_t>
    Eigen::VectorXd solve_(const XML& conf, DomainDiscretization<vec_t>& d, HDF& file, Timer& timer,
                           const approx_t& approx) {

//        auto idx = d.positions().filter([](const vec_t& p) { return p.norm() < 1e-4; });
//        d.removeNodes(idx);
        int N = d.size();
        prn(N);

        timer.addCheckPoint("shapes");
        prn("shapes");

        auto storage = d.computeShapes(approx);

        timer.addCheckPoint("matrix");
        prn("matrix");

        double alpha = conf.get<double>("case.alpha");

        SparseMatrix<double, RowMajor> M(N, N);
        Eigen::VectorXd rhs = Eigen::VectorXd::Zero(N);

        auto op = storage.implicitOperators(M, rhs);
        M.reserve(storage.supportSizes());

        for (int i : d.interior()) {
            double r = d.pos(i).norm();
            double rhs;
            if (vec_t::dim == 2) {
                rhs = (alpha - r) * std::cos(1.0 / (alpha + r)) / r / std::pow(alpha + r, 3);
            } else if (vec_t::dim == 3) {
                rhs = 2 * alpha * std::cos(1.0 / (alpha + r)) / r / std::pow(alpha + r, 3);
            }
            -op.lap(i) + (-std::pow(alpha+r, -4))*op.value(i) = rhs;
        }
        for (int i : d.boundary()) {
            op.value(i) = analytical(d.pos(i), alpha);
        }

//        out_file.atomic().writeSparseMatrix("M", M);
//        out_file.atomic().writeDoubleArray("rhs", rhs);

        PardisoLU<SparseMatrix<double>> solver;
        SparseMatrix<double> M2(M); M2.makeCompressed();
        timer.addCheckPoint("compute");
        prn("compute");
        solver.compute(M2);
        timer.addCheckPoint("solve");
        prn("solve");

        VectorXd sol = solver.solve(rhs);

        timer.addCheckPoint("postprocess");
        file.atomic().writeEigen("sol", sol);

        return sol;
    }

};


#endif //ADAPTIVE3D_POINT_CONTACT_CASE_HPP
