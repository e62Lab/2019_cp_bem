#include "lshape_case.hpp"
#include "estimators.hpp"
#include "adaptive_solver.hpp"

#ifdef NDEBUG
#warning "This is a release build!"
#else
#warning "This is a debug build!"
#endif


int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    LshapeSolver solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    BoxShape<Vec2d> box1(-1, 1);
    BoxShape<Vec2d> box2(0, {2, -2});
    auto shape = box1 - box2;

    Timer t;
    t.addCheckPoint("begin");

    DeviationEstimator estimator;

    double maxdx = conf.get<double>("num.maxdx");
    auto largest = [&](const Vec2d& v) { return maxdx; };

    solve_with_estimator_move(conf, shape, solver, estimator, file, largest);

    t.addCheckPoint("end");
    file.setGroupName("/");
    file.atomic().writeDoubleAttribute("total-time", t.duration("begin", "end"));
    prn(t.duration("begin", "end"));


    return 0;
}
