#include <iostream>
#include <vector>
#include <medusa/Medusa_fwd.hpp>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/SparseLU>

using namespace std;
using namespace mm;
using namespace Eigen;

/**
 * Scattered node interpolant averaging over small neighbourhood.
 * Implements modified Sheppard's method.
 */
template <class vec_t, class value_t>
class ModifiedSheppardsScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ModifiedSheppardsScatteredInterpolant(const Range<vec_t>& pos, int num_closest)
            : tree(pos), num_closest(num_closest) {}

    ModifiedSheppardsScatteredInterpolant(int num_closest)
            : num_closest(num_closest) {}

    template <typename values_t>
    void setValues(const values_t& new_values) { values = Range<value_t>(new_values.begin(), new_values.end()); }

    void setPositions(const Range<vec_t>& pos) { tree.resetTree(pos); }

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        Range<int> idx;
        Range<scalar_t> dists2;
        std::tie(idx, dists2) = tree.query(point, num_closest);
        if (dists2[0] < 1e-6) return values[idx[0]];
        value_t avg = value_t(0);
        scalar_t weight_sum = scalar_t(0);
        scalar_t R = std::sqrt(dists2.back());
        for (int i = 0; i < idx.size(); ++i) {
            scalar_t rel_dist = (1 - std::sqrt(dists2[i]) / R);
            scalar_t weight = rel_dist*rel_dist / dists2[i];
            avg += weight*values[idx[i]];
            weight_sum += weight;
        }
        return avg / weight_sum;
    }
};

template <typename vec_t, typename func_t>
ScalarFieldd compute_new_dx(const DomainDiscretization<vec_t>& d,
                            const ScalarFieldd& error, double eps, double eta, double alpha, double beta, func_t dxu,
                            HDF& file) {
    int ref = 0, deref = 0, same = 0, limit = 0;
    double min_e = error.minCoeff();
    double max_e = error.maxCoeff();

    int N = d.size();
    assert_msg(N == error.size(), "Domain size %d must match error size %d.", N, error.size());
    ScalarFieldd new_dx(N);
    for (int i = 0; i < N; ++i) {
        double old_dx = d.dr(i);
        if (error(i) >= eps) {
            double badness = (error(i) - eps) / (max_e - eps);  // in [0, 1]
            new_dx[i] = old_dx / (badness * (alpha - 1) + 1);
            ref++;
        } else if (error(i) < eta) {
            double badness = (eta - error(i)) / (eta - min_e);
            new_dx[i] = old_dx / (1+badness*(1.0/beta - 1));
            if (new_dx[i] < old_dx) { prn("Derefine is wrong."); exit(1); }
            double ll = dxu(d.pos(i));
            if (new_dx[i] > ll) {
                new_dx[i] = ll;  limit++;
            } else { deref++; }
        } else {
            new_dx[i] = old_dx;
            same++;
        }
    }

    std::cerr << ref << " nodes refined, "
              << deref << " derefined, "
              << limit << " hit derefine limit, "
              << same << " left same.\n";

    file.atomic().writeIntAttribute("ref", ref);
    file.atomic().writeIntAttribute("deref", deref);
    file.atomic().writeIntAttribute("same", same);
    file.atomic().writeIntAttribute("limit", limit);

    return new_dx;
}

void save_solution(HDF& out_file, const VectorXd& solution) {
    out_file.atomic().writeDoubleArray("sol", solution);
}
void save_solution(HDF& out_file, const Range<array<double, 3>>& solution) {
    out_file.atomic().writeDouble2DArray("sol", solution);
}
void save_solution(HDF& out_file, const pair<VectorField2d, VectorField<double, 3>>& solution) {
    out_file.atomic().writeDouble2DArray("stress", solution.second);
    out_file.atomic().writeDouble2DArray("displ", solution.first);
}
void save_solution(HDF& out_file, const pair<VectorField3d, VectorField<double, 6>>& solution) {
    out_file.atomic().writeEigen("stress", solution.second);
    out_file.atomic().writeEigen("displ", solution.first);
}

template <typename vec_t, typename Estimator, typename Solver, typename Func>
void solve_with_estimator_move(const XML& conf, const DomainShape<vec_t>& shape, Solver& solver, Estimator& e,
                               HDF& out_file, const Func& largest_allowed) {

    /*
    int riter = conf.get<int>("relax.riter");
    int num_neighbours = conf.get<int>("relax.num_neighbours");
    double init_heat = conf.get<double>("relax.init_heat");
    double final_heat = conf.get<double>("relax.final_heat");
    BasicRelax relax;
    relax.iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT).numNeighbours(num_neighbours)
            .initialHeat(init_heat).finalHeat(final_heat); */

    double global_error_tolerance = conf.get<double>("est.global_error_tolerance");
    double err_threshold = global_error_tolerance;
    double err_threshold_lower = conf.get<double>("est.derefine_error_tolerance");
    double aggressiveness = conf.get<double>("est.aggressiveness");
    double aggressiveness_deref = conf.get<double>("est.aggressiveness_deref");

    int total_max_iter = conf.get<int>("est.total_max_iter");
    int dx_num_closest = conf.get<int>("est.scattered_interpolant_neighbours");

    ModifiedSheppardsScatteredInterpolant<vec_t, double> interpolant(dx_num_closest);
    std::function<double(vec_t)> density = largest_allowed;

    int seed = conf.get<int>("num.seed");
    GeneralFill<vec_t> fill; fill.seed(seed);


    for (int iter = 0; iter < total_max_iter; ++iter) {
        Timer t;
        t.addCheckPoint("start");

        auto domain = shape.discretizeWithDensity(density, &fill);
//        DomainDiscretization<vec_t> domain = pre_relax_domain;
//        domain.relax(relax, density);

        int N = domain.size();
        int support_size = conf.get<int>("approx.n");
//        FindBalancedSupport find_support(support_size, 2*support_size);
        domain.findSupport(FindClosest(support_size));

//        if (N > 1e5) {
//            out_file.reopenFile();
//            out_file.reopenFolder();
//            out_file.setIntAttribute("converged",  0);
//            out_file.closeFile();
//            break;
//        }

        out_file.setGroupName(format("/%03d", iter));
        out_file.atomic().writeIntAttribute("N", N);
        out_file.atomic().writeDomain("domain", domain);


        auto solution = solver.solve(domain, conf, out_file, t);
//        save_solution(out_file, solution);

        VectorXd error = e.estimate(domain, solution);

        out_file.atomic().writeDoubleArray("error_indicator", error);
        prn(error.maxCoeff());
        if (error.mean() < global_error_tolerance) {
            cout << "Converged after " << iter << " iterations." << endl;
            return;
        }

//        domain.findSupport(FindClosest(2));
        ScalarFieldd new_dx = compute_new_dx(domain, error, err_threshold,
                err_threshold_lower, aggressiveness, aggressiveness_deref, largest_allowed, out_file);

        prn(new_dx.minCoeff())

        out_file.atomic().writeDoubleArray("dx", new_dx);

        interpolant.setPositions(domain.positions());
        interpolant.setValues(new_dx);
        density = std::ref(interpolant);
        t.addCheckPoint("end");
        prn(t.duration("start", "end"));
    }

    cout << "Stopped after " << total_max_iter << " iterations." << endl;
}
