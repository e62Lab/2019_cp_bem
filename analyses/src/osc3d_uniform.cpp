#include "osc_case.hpp"

int main(int argc, char* argv[]) {
    if (argc < 2) { print_red("Supply parameter file as the second argument.\n"); return 1; }
    XML conf(argv[1]);
    OscSolver<Vec3d> solver;
    solver.preprocess(conf);

    string output_file = conf.get<string>("meta.file");
    HDF file(output_file, HDF::DESTROY);
    file.writeXML("conf", conf);
    file.close();

    BoxShape<Vec3d> shape(0, 1);

    vector<string> ns = split(conf.get<string>("num.nxs"), ",");
    int iter = 0;
    for (const string& s : ns) {
        int nx = stoi(s);
        cout << "-------- iter #" << iter << ", nx = " << nx << " -----------\n";
        file.setGroupName(format("%02d", iter));

        Timer timer;
        timer.addCheckPoint("domain");

        double dx = 1.0/nx;
        GeneralFill<Vec3d> fill; fill.seed(1337);
        DomainDiscretization<Vec3d> d = shape.discretizeWithDensity([=](const Vec3d&) { return dx; }, &fill);

        int ss = conf.get<int>("approx.n");
        d.findSupport(FindClosest(ss));

        file.atomic().writeDomain("domain", d);

        solver.solve(d, conf, file, timer);

        ++iter;
    }

    return 0;
}
