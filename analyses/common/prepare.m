clear
close all
format compact
datapath = '/mnt/data/ijs/papers/2019_CP_BEM/analyses/';
plotdatapath = 'plotdata/';
paramspath = 'params/';
imagepath = 'images/';
exportpath = 'exportdata/';
