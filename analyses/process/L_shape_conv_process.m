prepare

casename = 'L_shape_uniform';
% casename = 'L_shape_adaptive';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

omega = 3*pi/2;

nerr = 3;
l1 = 1;
l2 = 2;
linf = 3;
errleg = {'$e_1$', '$e_2$', '$e_\infty$'};

errors = nan(ng, nerr);
Ns = nan(ng, 1);
for g = 1:ng
    gname = info.Groups(g).Name;

    pos = h5read(file, [gname '/domain/pos']);
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(x);
    sol = h5read(file, [gname '/sol']);
    asol = L_anal(x, y, omega);
    
    Ns(g) = N;
    
    errors(g, l1) = norm(sol-asol, 1) / norm(asol, 1);
    errors(g, l2) = norm(sol-asol, 2) / norm(asol, 2);
    errors(g, linf) = norm(sol-asol, 'inf') / norm(asol, 'inf');
    
end

save([plotdatapath casename '_error.mat'], 'errors', 'l1', 'l2', 'linf', 'Ns', 'nerr', 'errleg');