prepare

% casename = 'osc3d_uniform';
casename = 'osc2d_adaptive';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

alpha = h5readatt(file, '/conf', 'case.alpha');
gname = info.Groups(ng).Name;

pos = h5read(file, [gname '/domain/pos']);
x = pos(:, 1);
y = pos(:, 2);

N = length(x);
sol = h5read(file, [gname '/sol']);

save([plotdatapath casename '_sol.mat'], 'x', 'y', 'sol');