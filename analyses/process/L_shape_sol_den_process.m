prepare

casename = 'L_shape_adaptive';
file = [datapath casename '.h5'];
info = h5info(file);

ng = length(info.Groups) - 1;

omega = 3*pi/2;

errs = cell(ng, 1);
Ns = zeros(ng, 1);
dens = cell(ng, 1);
poss = cell(ng, 1);

for g = 1:ng
    gname = info.Groups(g).Name;

    pos = h5read(file, [gname '/domain/pos']);
    poss{g} = pos;
    x = pos(:, 1);
    y = pos(:, 2);
    N = length(x);
    sol = h5read(file, [gname '/sol']);
    asol = L_anal(x, y, omega);
    
    errs{g} = log10(abs(sol-asol));
    [~, d] = knnsearch(pos, pos, 'K', 4);
    dens{g} = mean(d(:, 2:4), 2);
    
    Ns(g) = N;
    
end

save([plotdatapath casename '_iters.mat'], 'ng', 'Ns', 'errs', 'dens', 'poss');