prepare

casename = 'L_shape_adaptive';

load([plotdatapath casename '_iters.mat']);

sizes = [5 5 5 3 3 2 2 2];

f1 = setfig('a1', [600 1200]);

for g = 1:ng

    x = poss{g}(:, 1);
    y = poss{g}(:, 2);

%     subplot(ng, 3, 3*g-2);
%     text(0.5, 0.5, sprintf('iteration %d', g))
%     axis off

%     title(sprintf('iteration %d', g))
    
    subplot(ng, 2, 2*g-1);
    scatter(x, y, sizes(g), errs{g}, 'filled');
    axis equal
    colormap jet
    colorbar
    caxis([-9 -1])
    xlim([-1 1])
    ylim([-1 1])
    text(140, 70, sprintf('iter %d', g), 'Units', 'points')
    text(125, 55, sprintf('$N = %d$', Ns(g)), 'Units', 'points')
    
    subplot(ng, 2, 2*g);
    d = dens{g};
    scatter(x, y, sizes(g), -log2(d/max(d)), 'filled');
    axis equal
    colormap jet
    colorbar
    xlim([-1 1])
    ylim([-1 1])
    
%     caxis([0 1.2])
end

% exportf(f1, [imagepath 'L_shape_progress.png'], '-m3')