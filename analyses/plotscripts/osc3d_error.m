prepare


% casenameU = 'osc2d_uniform_error';
casenameA = 'osc3d_adaptive_error';

A = load([plotdatapath casenameA '.mat']);

f1 = setfig('a1', [500 400]);
ll;
xlim([1e2 1e6]);
ylim([1e-4 1e2]);
leg = {};
for i = 1:A.nerr
    plot(A.Ns, A.errors(:, i), 'o-'); leg{end+1} = A.errleg{i};
%     k = polyfit(log(A.Ns), log(A.errors(:, i)), 1);
%     plot(A.Ns, exp(k(2)).*A.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end 
legend(leg, 'Location', 'SW');

exportf(f1, [imagepath casenameA '.png'], '-m4')
