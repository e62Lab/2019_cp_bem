prepare

casename = 'osc3d_adaptive';

load([plotdatapath casename '_sol.mat'], 'x', 'y', 'z', 'sol');

f1 = setfig('a2', [500 400]);
view(3)
scatter3(x, y, z, 10, sol, 'filled');
daspect([1 1 1])
caxis([-1 1])
title('Solution $u$')
colormap jet
colorbar

exportf(f1, [imagepath casename '_sol.png'], '-m4')

[~, d] = knnsearch([x y z], [x y z], 'K', 4);
d = mean(d(:, 2:4), 2);
f2 = setfig('a4', [500 400]);
view(3)
scatter3(x, y, z, 10, -log2(d/max(d)), 'filled');
daspect([1 1 1])
colormap jet
colorbar
title('$-\log_2(d/d_{max})$')

% exportf(f2, [imagepath casename '_den.png'], '-m4')