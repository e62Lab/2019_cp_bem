prepare


casenameU = 'osc2d_uniform_error';
casenameA = 'osc2d_adaptive_error';

A = load([plotdatapath casenameA '.mat']);
U = load([plotdatapath casenameU '.mat']);



f1 = setfig('a1', [500 400]);
ll;
xlim([1e2 1e6]);
ylim([1e-4 1e2]);
leg = {};
for i = 1:A.nerr
    plot(A.Ns, A.errors(:, i), 'o-'); leg{end+1} = A.errleg{i};
%     k = polyfit(log(A.Ns), log(A.errors(:, i)), 1);
%     plot(A.Ns, exp(k(2)).*A.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end 
legend(leg, 'Location', 'SW');


f2 = setfig('a3', [500 400]);
ll;
xlim([1e2 1e6]);
ylim([1e-4 1e2]);
leg = {};
for i = 1:U.nerr
    plot(U.Ns, U.errors(:, i), 'o-'); leg{end+1} = U.errleg{i};
    
%     k = polyfit(log(U.Ns), log(U.errors(:, i)), 1);
%     plot(U.Ns, exp(k(2)).*U.Ns.^k(1), 'k--'); leg{end+1} = sprintf('$k=%.2f$', k(1));
end
legend(leg, 'Location', 'SW');

exportf(f1, [imagepath casenameA '.png'], '-m4')
exportf(f2, [imagepath casenameU '.png'], '-m4')
