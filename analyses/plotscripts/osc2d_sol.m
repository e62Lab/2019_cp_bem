prepare

casename = 'osc2d_adaptive';

load([plotdatapath casename '_sol.mat'], 'x', 'y', 'sol');

den = 100;
[X, Y] = meshgrid(deal(linspace(0, 1, den)));
S = griddata(x, y, sol, X, Y);

f1 = setfig('a2', [500 400]);
contourf(X, Y, S, 100, 'EdgeColor', 'none');
daspect([1 1 1])
caxis([-1 1])
title('Solution $u$')
colormap jet
colorbar

[~, d] = knnsearch([x y], [x y], 'K', 4);
d = mean(d(:, 2:4), 2);
D = griddata(x, y, -log2(d/max(d)), X, Y);

f2 = setfig('a4', [500 400]);
contourf(X, Y, D, 100, 'EdgeColor', 'none');
daspect([1 1 1])
caxis([0, inf])
colormap jet
colorbar
title('$-\log_2(d/d_{max})$')



% exportf(f1, [imagepath casename '_sol.png'], '-m4')
% exportf(f2, [imagepath casename '_den.png'], '-m4')