% !TeX spellcheck = en_US
\documentclass{beamer}

\usefonttheme[onlymath]{serif}

\usepackage[slovene]{babel}    % slovenian language and hyphenation
\usepackage[T1]{fontenc}       % make čšž work on output
\usepackage[utf8]{inputenc}
\usepackage{url}               % \url and \href for links
\usepackage{units}
\usepackage{bibentry}

\newtheorem{izrek}{Izrek}
\newtheorem{posledica}{Posledica}

\theoremstyle{definition}
\newtheorem{definicija}{Definicija}
\newtheorem{opomba}{Opomba}
\newtheorem{zgled}{Zgled}

% basic sets
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\T}{\ensuremath{\mathsf{T}}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\X}{\mathcal{X}}
\renewcommand{\b}{\boldsymbol}

% greek letters
\let\oldphi\phi
\let\oldtheta\theta
\newcommand{\eps}{\varepsilon}
\renewcommand{\phi}{\varphi}
\renewcommand{\theta}{\vartheta}

% vektorska analiza
\newcommand{\grad}{\operatorname{grad}}
\newcommand{\rot}{\operatorname{rot}}
\renewcommand{\div}{\operatorname{div}}
\newcommand{\dpar}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\bl}{\boldsymbol}

\usetheme[outer/progressbar=foot]{metropolis}

\institute{``Jožef Stefan'' Institute,
  Parallel and Distributed Systems Laboratory \\[3ex] 2.\ 7.\ 2019, BEM/MRM 42}
\title{Adaptive RBF-FD method for Poisson's equation}
\date{}
\author{\textbf{Jure Slak}, Gregor Kosec}
\hypersetup{pdftitle={Adaptive RBF-FD method for Poisson's equation}, pdfauthor={Jure Slak}, 
pdfsubject={}, pdfkeywords={}}  % setup pdf metadata

\newcommand{\ijs}{\hfill JSI \raisebox{-4px}{\includegraphics[height=16px]{ijs_logo_inverse.pdf}}}

% \pagestyle{empty}              % vse strani prazne
% \setlength{\parindent}{0pt}    % zamik vsakega odstavka
% \setlength{\parskip}{10pt}     % prazen prostor po odstavku
\setlength{\overfullrule}{0pt}  % oznaci predlogo vrstico z veliko črnine
%\usepackage{libertine}

\definecolor{Purple}{HTML}{911146}
\definecolor{Orange}{HTML}{CF4A30}
\definecolor{Dark}{HTML}{333333}
\setbeamercolor{frametitle}{bg=Dark}

\setbeamercolor{alerted text}{fg=Orange}
\setbeamercolor{normal text}{fg=Dark}

\metroset{block=fill}

\begin{document}

\begin{frame}
\maketitle
\end{frame}

%\begin{frame}{Solving steady state problems \ijs}
%\begin{itemize}
%  \item Classical approaches: \\ 
%  Finite Difference Method, Finite Element Method \\[2ex]
%  \includegraphics[width=0.37\linewidth]{images/fdm.png} \hspace{0.7em}
%  \includegraphics[width=0.4\linewidth]{images/mesh.pdf}
%  \item Problems: inflexible geometry, mesh generation
%  \item Response: mesh-free methods (EFG, MLPG, FPM)
%\end{itemize}
%\end{frame}

\begin{frame}{Overview \ijs}
  \begin{enumerate} \setlength{\parskip}{10pt}
    \item Adaptivity
      \begin{enumerate}
        \item Solution procedure
        \item Discretization 
        \item Error estimator
        \item Refinement
      \end{enumerate}
    \item $L$-shaped domain
    \item Helmholtz equation
    \item Additional examples
  \end{enumerate}
\end{frame}

\begin{frame}{Adaptivity \ijs}
  \textbf{Adaptive procedure:}
  
  Discretize domain $\Omega$ to obtain discretization $\X^{(0)}$\\
  For $j = 0, \ldots, I_{max}$ 
  \begin{enumerate}
    \item Solve the problem and obtain $u^{(j)}$
    \item Estimate errors $e^{(j)}$
    \item If $\|e^{(j)}\| < \eps$, return $u^{(j)}$
    \item Refine the discretization $\X^{(j)}$ using $e^{(j)}$ to obtain $\X^{(j+1)}$ 
  \end{enumerate}
\end{frame}


\begin{frame}{Solution procedure -- strong form meshless methods \ijs}
Domain discretization: 
\begin{columns}
  \begin{column}{0.5\linewidth}
    \begin{itemize} 
      \item Points $x_i$ on the boundary and in the interior
      \item Point neighborhoods $N(x_i)$ 
    \end{itemize}
  \end{column}
\begin{column}{0.45\linewidth}
  \includegraphics[width=1\linewidth]{images/drawings/domain_theoretical.pdf}
\end{column}
\end{columns}
Classical Finite Differences:
\[
u''(x_i) \approx \frac{1}{h^2} u(x_{i-1}) - \frac{2}{h^2} u(x_i) + \frac{1}{h^2} u(x_{i+1})
\]
Generalized Finite Differences:
\[
(\L u)(x_i) \approx \sum_{x_j \in N(x_i)} w_j^i u(x_j)
\]
+ exactness for a certain set of functions (e.g.\ monomials)
\end{frame}

\begin{frame}{Solution procedure -- RBF-FD \ijs}
  Exactness is imposed for Radial Basis Functions
  \begin{columns}
    \begin{column}{0.65\linewidth}
      \begin{itemize}
        \item Given nodes $X = \{x_1, \ldots, x_n\}$ and 
        a radial function $\phi = \phi(r)$
        \item Generate $\{\phi_i := \phi(\|\cdot - x_i\|), x_i \in X \}$
      \end{itemize}
    \end{column}
    \begin{column}{0.4\linewidth}
      \includegraphics[width=\linewidth]{images/rbfs.png}
    \end{column}
  \end{columns}
  \begin{center}
  \end{center}
\vspace{-5ex}
Imposing exactness of 
  \[
  (\L u)(x_i) \approx \sum_{x_j \in N(x_i)} w_j^i u(x_j)
  \]
  for each $\phi_j$ for $x_j \in N(x_i)$, we get
  \begingroup
  \arraycolsep2pt
  \[
  \begin{bmatrix}
  \phi(\|x_{j_1} - x_{j_1}\|) & \cdots & \phi(\|x_{j_{n_i}} - x_{j_1}\|) \\
  \vdots & \ddots & \vdots \\
  \phi(\|x_{j_1} - x_{j_{n_i}}\|) & \cdots & \phi(\|x_{j_{n_i}} - x_{j_{n_i}}\|) \\
  \end{bmatrix}
  \begin{bmatrix}
  w^i_{j_1} \\ \vdots \\ w^i_{j_{n_i}}
  \end{bmatrix}
  =
  \begin{bmatrix}
  (\L \phi_{j_1})(x_i) \\
  \vdots \\
  (\L \phi_{j_{n_i}})(x_i) \\
  \end{bmatrix}
  \label{eq:matrix}
  \]
  \endgroup
\end{frame}

\begin{frame}{Sol.\ proc.\ -- RBF-FD + Monomial augmentation \ijs}
  Enforce consistency up to certain order, e.g. for constants
  \[
    \begin{bmatrix}
      A & \b 1 \\ \b 1^\T & 0
    \end{bmatrix}
    \begin{bmatrix}
      \b w \\ \lambda
    \end{bmatrix}
    =
    \begin{bmatrix}
      \b \ell_{\phi} \\ 0
    \end{bmatrix}
  \]
  In general:
\[
  \begin{bmatrix}
  A & P \\
  P^\T & 0
  \end{bmatrix}
  \begin{bmatrix}
  \b w \\ \b \lambda
  \end{bmatrix}
  =
  \begin{bmatrix}
  \b \ell_{\phi} \\ \b \ell_{p}
  \end{bmatrix},
  \]
  where
\[
  P = \begin{bmatrix}
  p_1(\b x_1) & \cdots & p_s(\b x_1) \\
  \vdots & \ddots & \vdots        \\
  p_1(\b x_n) & \cdots & p_s(\b x_n) \\
  \end{bmatrix}, \quad
  \b \ell_p = \begin{bmatrix}
  (\L p_1)|_{\b x=\b x^\ast} \\
  \vdots \\
  (\L p_s)|_{\b x=\b x^\ast} \\
  \end{bmatrix}.
\]
\end{frame}

\begin{frame}{Solution procedure -- overview \ijs}
\vspace{2ex}
Problem:
\begin{columns}
  \begin{column}{0.5\linewidth}
    \vspace{-8ex}
    \begin{align*}
    \L u = f \quad &\text{ on } \Omega,  \\
    u = u_0 \quad &\text{ on } \partial \Omega,
    \end{align*}
  \end{column}
  \begin{column}{0.5\linewidth}
    \includegraphics[width=0.9\linewidth]{images/adv-dif.png}
  \end{column}
\end{columns}
\vspace{-7ex}
\begin{enumerate}
  \item Discretize domain $\Omega$
  \item Find neighborhoods $N(x_i)$
  \item Compute weights $\boldsymbol{w}^i$ for approximation of $\L$ over $N(x_i)$
  \item Assemble weights in a sparse system $Wu = f$
  \item Solve the sparse system  $Wu = f$
  \item Approximate/interpolate the solution 
\end{enumerate}
\end{frame}

%
%\begin{frame}{Test case: Poisson problem \ijs} 
%  \begin{columns}
%    \begin{column}{0.5\linewidth}
%      \vspace{-8ex}
%      \begin{align*}
%      \L u = f \quad &\text{ on } \Omega,  \\
%      u = u_0 \quad &\text{ on } \partial \Omega,
%      \end{align*}
%      Domain $\Omega$ is an annulus.
%      
%      PHS are used: $\phi(r) = r^3$. \\
%      
%      Augmentation up to order $m$. \\
%      
%      Maximally 45 monomials.
%      
%      65 closest neighbours.
%    \end{column}
%    \begin{column}{0.5\linewidth}
%      \includegraphics[width=0.9\linewidth]{images/laplace_phs_2d_domain.pdf}
%    \end{column}
%  \end{columns}
%\end{frame}

\begin{frame}{Test case: Poisson problem \ijs}
  \begin{columns}
    \begin{column}{0.3\linewidth}
      Annulus domain, scattered nodes \\[1cm]
      
      convergence orders match augmentation \\[1cm]
      
       $\ell_1$ and $\ell_2$ errors similar
    \end{column}
    \begin{column}{0.8\linewidth}
      \includegraphics[width=0.9\linewidth]{images/laplace_phs_2d_error.pdf}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Discretization \ijs}
  Fill domain $\Omega$ with locally regular nodes according to 
  arbitrary spacing function $h(p)$.
  
  Recent algorithms for variable density node positioning in 3D: \\
  {\footnotesize
  Slak \& Kosec, 2018: \url{https://arxiv.org/abs/1812.03160} \\
  van der Sande \& Fornberg, 2019: \url{https://arxiv.org/abs/1906.00636}}
  
  \includegraphics[height=4cm]{images/complex_boundary.pdf}
  \includegraphics[height=4cm]{images/complex_boundary_3d.pdf}
\end{frame}

\begin{frame}{Error indicator and refinement \ijs}
  Simple error indicator:
  \begin{align*}
    \hat{u}(x_i) &= \frac1n \sum_{x_j \in N(x_i)} u(x_j) \\
    e_i^2 &= \frac1n \sum_{x_j \in N(x_i)} |\hat{u}(x_i) - u(x_j)|^2     
  \end{align*}
  
  Refinement by appropriate modification of $h$. Three cases:
  \[
    \begin{cases}
    \text{increase} & \text{if } e_i > \eps \\
    \text{no change} & \text{if } \eta \leq e_i \leq \eps \\
    \text{decrease} & \text{if } e_i < \eta 
    \end{cases}  
  \]
  
Increase/decrease proportional to $e_i$, maximal change for factor $\alpha$.
\end{frame}

\begin{frame}{$\b L$-shaped domain -- convergence\ijs}
  \hspace*{\fill} Uniform \hfill \hfill Adaptive \hspace*{\fill}
  
  \includegraphics[height=4.2cm]{images/L_shape_uniform_error.png}
  \includegraphics[height=4.2cm]{images/L_shape_adaptive_error.png}
\end{frame}

\begin{frame}{$\b L$-shaped domain  \ijs}
  Adaptive iteration: error and node density $\rho = -\log_2 \frac{d_i}{\max_j d_j}$. \\[2ex]
  
  \includegraphics[height=5.2cm]{images/L_shape_progress1.png}
  \includegraphics[height=5.2cm]{images/L_shape_progress2.png}
\end{frame}

\begin{frame}{Helmholtz equation \ijs}
  \[
    -\nabla^2 u + \frac{1}{(r+\alpha)^4} = f, \qquad  u(r) = \sin\left(\frac{1}{\alpha+r}\right) 
  \]
  
  \hspace*{\fill} 2D \hfill \hfill 3D \hspace*{\fill}
    
  \includegraphics[height=4cm]{images/osc2d_adaptive_error.png}
  \includegraphics[height=4cm]{images/osc3d_adaptive_error.png}
\end{frame}


\begin{frame}{Helmholtz equation \ijs}
  \includegraphics[height=4cm]{images/osc2d_adaptive_sol.png}
  \includegraphics[height=4cm]{images/osc2d_adaptive_den.png}
  \includegraphics[height=4cm]{images/osc3d_adaptive_sol.png}
  \includegraphics[height=4cm]{images/osc3d_adaptive_den.png}
\end{frame}

%\begin{frame}{Test case: Scattering problem \ijs}
%  Anisotropic cylindrical scatterer. Let $v$ be the (complex-valued) field 
%  inside the scatterer and $u = u^s + u^i$ outside. 
%  
%  \begin{center}
%    \includegraphics[width=0.60\linewidth]{images/ScatteringDiagram.pdf}
% \end{center}
%\end{frame}


%\begin{frame}{Test case: Scattering problem}
%  Discretized model:
%  \begin{align*}
%  \nabla \cdot A_\mu \nabla v + \epsilon_r k^2 \thinspace v = 0 \qquad
%  &\textnormal{in} \quad D,  \\
%  \nabla^2 u^s + k^2 \thinspace u^s  = 0 \qquad &\textnormal{in} \quad \Omega
%  \setminus D,
%  \end{align*}
%  with boundary conditions
%  \begin{align*}
%  v - u^s =u^i  \qquad &\textnormal{on} \quad \partial D, \\
%  \dpar{v}{\vec n_{A_\mu}\!\!\!} - \dpar{u^s}{\vec n} = \dpar{u^i}{\vec n}
%  \qquad
%  &\textnormal{on}
%  \quad  \partial D, \label{eq:BC2} \\
%  \dpar{u^s}{\vec n} + \left( ik + \frac{1}{2r_2} \right)u^s = 0  \qquad & \textnormal{on} \quad 
%  \partial \Omega.
%  \end{align*}  
%\end{frame}
%
%\begin{frame}{Test case: Scattering problem \ijs}
%  Around 90\,000 nodes, augmentation of order 4. 
%
%  \centering
%  \includegraphics[width=0.45\linewidth]{images/InsideField.png}
%  \includegraphics[width=0.45\linewidth]{images/OutsideField.png}
%  
%  \hspace*{\fill}  Magnitude of $v$. \hfill
%  Magnitude of $u^s$. \hspace*{\fill}
%\end{frame}

\begin{frame}{Additional examples \ijs}
  \includegraphics[width=0.8\linewidth]{images/Bou_sol.png} 
  \parbox[b]{0.18\linewidth}{\small \textbf{Support for:} complex numbers, ghost nodes, coupled 
  domains}
  
  \includegraphics[width=0.4\linewidth]{images/DVD_3D.png}
  \includegraphics[width=0.4\linewidth]{images/DVD_3D_irreg.png}
\end{frame}

\begin{frame}{Final remarks \ijs}
All computations were done using open source Medusa library. \\[4ex]

\begin{columns}  
  \begin{column}{0.2 \linewidth}
    \includegraphics[width=\linewidth]{images/logo_transparent.png}
  \end{column}
  \begin{column}{0.7\linewidth}
    \textbf{\Large Medusa} \\ 
    Coordinate Free  Mehless Method implementation \\
    \url{http://e6.ijs.si/medusa/}
  \end{column}
\end{columns}

\vspace{3ex}

Slides available at \url{http://e6.ijs.si/~jslak/}. \\
Thank you for your attention!

\vspace{3ex}

\footnotesize
\textbf{Acknowledgments:}
FWO Lead Agency project: G018916N Multi-analysis of fretting
fatigue using physical and virtual experiments, the ARRS research
core funding No.~P2-0095 and Young Researcher program PR-08346.
\end{frame}

\end{document}
% vim: syntax=tex
% vim: spell spelllang=sl
% vim: foldlevel=99
% Latex template: Jure Slak, jure.slak@gmail.com

